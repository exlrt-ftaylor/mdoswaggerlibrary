import Axios from 'axios';

import MdoService from '../src/MdoApi.service';
import Endpoint from '../src/Endpoint.class';
import { API_HOST } from '../src/constants';

import { getUserEndpointData, mdoExternalLMAEndpoint, getRegionArticleListEndpoint } from './endpointData';

describe('MdoApiService', () => {
  describe('static members', () => {
    describe('baseurl', () => {
      it('returns a URL object', () => {
        const url = MdoService.getBaseUrl();

        expect(url).toBeInstanceOf(URL);
      });
      it('returns the local/dev url by default ', () => {
        const url = MdoService.getBaseUrl();

        expect(url.href).toBe(`http://${API_HOST}/rest`);
      });
      it('can return a different host if sent one ', () => {
        const url = MdoService.getBaseUrl('ttgtgttg.kp.org');

        expect(url.href).toBe('http://ttgtgttg.kp.org/rest');
      });
    });
    describe('axiosInstance', () => {
      it('generates an axios instance', () => {
        const axiosInstance = MdoService.getAxiosInstance();

        expect(axiosInstance).toBeInstanceOf(Function);
      });
      it('can have an empty url', () => {
        const axiosInstance = MdoService.getAxiosInstance();

        expect(axiosInstance.defaults.baseURL).toBe('');
      });
      it('has a baseUrl if we give it', () => {
        const axiosInstance = MdoService.getAxiosInstance('http://foo.bar');

        expect(axiosInstance.defaults.baseURL).toBe('http://foo.bar');
      });
    });
    describe('getApiInfo', () => {
      it('gets the swagger info', async () => {
        const baseUrl = MdoService.getBaseUrl(API_HOST);
        const axios = MdoService.getAxiosInstance(baseUrl.href, Axios);
        const data = await MdoService.getApiInfo(axios);
        expect(data).toHaveProperty('swagger', '2.0');
      });
    });
    describe('collision detection', () => {
      it('gets indexes of paths w/ identical endings', () => {
        const paths = [
          '/rest/mdoExternal/facility/create',
          '/rest/mdoExternal/provider/create',
          '/rest/mdoExternal/localMarketArea/create',
          'rest/mdoExternal/{region}/article/{locationType}/list',
        ];
        const indexesWSameEndings = MdoService.getIndexesOfPathsWithIdenticalEndings(paths);

        expect(indexesWSameEndings).toEqual(expect.arrayContaining([0, 1, 2]));
      });
      it('gets indexes of paths w/ identical endings', () => {
        const paths = [
          '/rest/mdoExternal/facility/create',
          '/rest/mdoExternal/provider/create',
          '/rest/mdoExternal/localMarketArea/create',
          'rest/mdoExternal/{region}/article/{locationType}/list',
          'rest/mdoExternal/{region}/faq/{locationType}/list',
          'rest/mdoExternal/{region}/form/{locationType}/list',
          'rest/mdoExternal/provider/terminate',
        ];
        const indexesWSameEndings = MdoService.getIndexesOfPathsWithIdenticalEndings(paths);

        expect(indexesWSameEndings).toEqual(expect.arrayContaining([0, 1, 2, 3, 4, 5]));
      });
      it('will look and paths and calculate how far to offset in order to get unique names', () => {
        const paths = [
          'rest/mdoExternal/{region}/article/{locationType}/list',
          'rest/mdoExternal/{region}/faq/{locationType}/list',
          'rest/mdoExternal/{region}/form/{locationType}/list',
        ];
        const lastUniquePathIndex = MdoService.getIndexOfLastUniquePathInPaths(paths);

        expect(lastUniquePathIndex).toEqual(3);
      });
      it('will look and paths and calculate how far to offset in order to get unique names', () => {
        const paths = [
          '/rest/mdoExternal/facility/create',
          '/rest/mdoExternal/provider/create',
          '/rest/mdoExternal/localMarketArea/create',
        ];
        const lastUniquePathIndex = MdoService.getIndexOfLastUniquePathInPaths(paths);

        expect(lastUniquePathIndex).toEqual(2);
      });
      it('will look at paths of different path sizes and give the offset for the largest', () => {
        const paths = [
          '/rest/mdoCommon/{region}/facilities/{facility}/getDisplayData',
          '/rest/mdoCommon/{region}/facilities/{facility}/subdepartments/{subDepartment}/getDisplayData',
        ];
        const lastUniquePathIndex = MdoService.getIndexOfLastUniquePathInPaths(paths);

        expect(lastUniquePathIndex).toEqual(6);
      });
    });
  });
  describe('constructor', () => {
    it('will have a baseUrl that matches current test environment', () => {
      const mdoApi = new MdoService();

      expect(mdoApi.baseURL.href).toBe(`http://${API_HOST}/rest`);
    });
    it('the https scheme can be set', () => {
      const mdoApi = new MdoService('foo', '/mdoTools', 'rest', 'https');
      mdoApi.apiScheme = 'https';

      expect(mdoApi.baseURL.href).toBe(`https://foo/rest`);
    });
    it('will have an axios instance that matches current test environment', () => {
      const mdoApi = new MdoService();

      expect(mdoApi.axios.defaults.baseURL).toBe(`http://${API_HOST}/rest`);
    });
    it('will have a default swaggerPath', () => {
      const mdoApi = new MdoService();

      expect(mdoApi).toHaveProperty('swaggerPath', '/mdoTools');
    });
    it('can be sent a custom swagger path', () => {
      const mdoApi = new MdoService(API_HOST, '/access');

      expect(mdoApi).toHaveProperty('swaggerPath', '/access');
    });
    it('prepends a / if it isn\'t given one', () => {
      const mdoApi = new MdoService(API_HOST, 'access');

      expect(mdoApi).toHaveProperty('swaggerPath', '/access');
    });
    it('can have the baseUrl just set in a method as a string', () => {
      const mdoApi = new MdoService();

      mdoApi.setBaseUrl(`http://${API_HOST}/cache`);
      expect(mdoApi.baseURL.toString()).toEqual(`http://${API_HOST}/cache`);
      expect(mdoApi.apiScheme).toEqual('http');
      expect(mdoApi.apiHost).toEqual(API_HOST);
      expect(mdoApi.basePath).toEqual('cache');
    });
    it('can have the baseUrl just set in a method as a URL', () => {
      const mdoApi = new MdoService();

      mdoApi.setBaseUrl(new URL('https://ttgstg-tcm.tpmg.kp.org/cache/'));
      expect(mdoApi.baseURL.toString()).toEqual('https://ttgstg-tcm.tpmg.kp.org/cache/');
      expect(mdoApi.apiScheme).toEqual('https');
      expect(mdoApi.apiHost).toEqual('ttgstg-tcm.tpmg.kp.org');
      expect(mdoApi.basePath).toEqual('cache');
    });
    it('can have the baseUrl just set in a method as a host', () => {
      const mdoApi = new MdoService();

      mdoApi.setBaseUrl('ttgstg-tcm.tpmg.kp.org');
      expect(mdoApi.baseURL.toString()).toEqual('http://ttgstg-tcm.tpmg.kp.org/rest');
      expect(mdoApi.apiScheme).toEqual('http');
      expect(mdoApi.apiHost).toEqual('ttgstg-tcm.tpmg.kp.org');
      expect(mdoApi.basePath).toEqual('rest');
    });
  });
  describe('methods', () => {
    describe('getApiInfo', () => {
      it('gets some API info', async () => {
        const mdoApi = new MdoService();
        const data = await mdoApi.getApiInfo();
        expect(data).toHaveProperty('swagger', '2.0');
        expect(data).toHaveProperty('info');
        expect(data.info).toHaveProperty('title', 'MDO Tools Rest API');
      });
    });
    describe('init', () => {
      it('gets all endpoints', async () => {
        const mdoApi = new MdoService();
        await mdoApi.init();
        expect(mdoApi).toHaveProperty('endpoints');
        expect(mdoApi.endpoints.length).toBeGreaterThan(0);
        expect(mdoApi.endpoints[0]).toBeInstanceOf(Endpoint);
      });
      it('can initialize on a different swagger instance and that will set a new swagger path', async () => {
        const mdoApi = new MdoService();
        await mdoApi.init('/access');
        expect(mdoApi).toHaveProperty('swaggerPath', '/access');
        expect(mdoApi).toHaveProperty('title', 'MDO Access Rest API');
        expect(mdoApi.endpoints.length).toBeGreaterThan(0);
        expect(mdoApi.endpoints[0]).toBeInstanceOf(Endpoint);
      });
      it('will have a map of methods', async () => {
        const mdoApi = new MdoService('ttgssqa0vmwap25.ttgtpmg.net:5000', '/mdoTools');
        await mdoApi.init();
        expect(mdoApi).toHaveProperty('swaggerPath', '/mdoTools');
        expect(mdoApi).toHaveProperty('methods');
        expect(mdoApi.methods.size).toBeGreaterThan(10);
      });
    });
    describe('setting endpoints', () => {
      it('can manually set a method', () => {
        const service = new MdoService();
        service.urlNamespace = 'cache';
        service.setBaseUrl('http://ttgstg-tcm.tpmg.kp.org/');
        service.setEndpoint('/cache/access/user', getUserEndpointData);

        expect(service.endpoints.length).toBeGreaterThan(0);
        expect(service.methods.has('getAccessUser')).toEqual(true);
      });
      it('will set an endpoint that has a long method name', () => {
        const service = new MdoService('http://ttgstg-tcm.tpmg.kp.org/', '/mdoExternal');
        const endpoint = service.setEndpoint('/rest/mdoExternal/localMarketArea/create', mdoExternalLMAEndpoint, true);

        expect(endpoint.endpointActions.has('postMdoExternalLocalMarketAreaCreate')).toEqual(true);
      });
      it('will set an endpoint that has a slightly longer method name with a bool', () => {
        const service = new MdoService('http://ttgstg-tcm.tpmg.kp.org/', '/mdoExternal', 'mdoExternal');
        const endpoint = service.setEndpoint(
          '/rest/mdoExternal/{region}/article/{locationType}/list',
          getRegionArticleListEndpoint,
          true,
        );
        expect(endpoint.endpointActions.has('getRegionLocationTypeList')).toEqual(true);
      });
      it('will give an endpoint methodname with an int of 1 that is same as a bool', () => {
        const service = new MdoService('http://ttgstg-tcm.tpmg.kp.org/', '/mdoExternal', 'mdoExternal');
        const endpoint = service.setEndpoint(
          '/rest/mdoExternal/{region}/article/{locationType}/list',
          getRegionArticleListEndpoint,
          1,
        );
        expect(endpoint.endpointActions.has('getRegionLocationTypeList')).toEqual(true);
      });
      it('will set an endpoint that has a longer method name', () => {
        const service = new MdoService('http://ttgstg-tcm.tpmg.kp.org/', '/mdoExternal', 'mdoExternal');
        const endpoint = service.setEndpoint(
          '/rest/mdoExternal/{region}/article/{locationType}/list',
          getRegionArticleListEndpoint,
          2,
        );
        expect(endpoint.endpointActions.has('getRegionArticleLocationTypeList')).toEqual(true);
      });
      it('will not duplicate endpoints with the same path', () => {
        const service = new MdoService('http://ttgstg-tcm.tpmg.kp.org/', '/mdoExternal', 'mdoExternal');
        service.setEndpoint(
          '/rest/mdoExternal/{region}/article/{locationType}/list',
          getRegionArticleListEndpoint,
          2,
        );
        service.setEndpoint(
          '/rest/mdoExternal/{region}/article/{locationType}/list',
          getRegionArticleListEndpoint,
          2,
        );
        expect(service.methods.size).toEqual(1);
      });
    });
    describe('automatic method collision detection', () => {
      it('has as many methods as it does endpoints', async () => {
        const mdoApi = new MdoService('ttgssqa0vmwap25.ttgtpmg.net:5000', '/mdoTools');
        await mdoApi.init();
        expect(mdoApi.methods.size).toBeGreaterThanOrEqual(mdoApi.endpoints.length);
      });
    });
    describe('using the methods', () => {
      it.skip('can get user groups', async () => {
        const service = new MdoService('ttgssqa0vmwap25.ttgtpmg.net:5000', '/mdoTools', 'mdoCommon');
        await service.init();
        const { methods } = service;
        const method = methods.get('getUserAvailableGroups');

        const data = await method({
          region: 'NCAL',
          group: 'local',
        });
        expect(data).toBeTruthy();
      });
      it('can use a manually set a method', async () => {
        const service = new MdoService();
        service.urlNamespace = 'cache';
        service.setBaseUrl('http://ttgstg-tcm.tpmg.kp.org/');
        service.setEndpoint('/cache/access/user', getUserEndpointData);

        const { methods } = service;
        const method = methods.get('getAccessUser');
        const data = await method({ name: 'D459062' });
        expect(data).toBeTruthy();
        expect(data).toHaveProperty('Name');
        expect(data).toHaveProperty('Uri');
        expect(data).toHaveProperty('Description');
      });
    });
  });
});
